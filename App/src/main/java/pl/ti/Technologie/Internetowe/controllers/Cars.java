package pl.ti.Technologie.Internetowe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.ti.Technologie.Internetowe.model.Reserve;
import pl.ti.Technologie.Internetowe.service.ReserveService;

@Controller
public class Cars {

    final private ReserveService reserveService;

    @Autowired
    public Cars(ReserveService reserveService) {
        this.reserveService = reserveService;
    }

    @GetMapping("/")
    public String start() {
        return "index.html";
    }


    @PostMapping("/formularz")
    public @ResponseBody String save(Reserve reserve) {
        return reserveService.addReserve(reserve);
    }
}
