package pl.ti.Technologie.Internetowe.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table
@Entity
@NoArgsConstructor
@Getter
@Setter
public class Reserve {

    @Id
    @GeneratedValue
    int id;
    String name;
    String surname;
    int phone;
    String carName;
    String startReservation;
    String endReservation;

    public Reserve(String name, String surname, int phone, String carName, String startReservation, String endReservation) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.carName = carName;
        this.startReservation = startReservation;
        this.endReservation = endReservation;
    }
}
